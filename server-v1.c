//This is a connection-oriented Iterative web server

#include <sys/ioctl.h>
#include <pwd.h>
#include <grp.h>
#include <time.h>
#include <netinet/in.h>
#include <stdio.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <dirent.h>
#include <errno.h>
#include <sys/ioctl.h>
#include <sys/stat.h>

#define MY_IP "192.168.85.129"
#define MY_PORT 49555
#define BACK_LOG 100
#define LINELEN 1024

extern int errno;
int argsCount=0;
char headerHtml[]="Http/1.1 200 OK\nContent-type: text/html\n\n";
char headerText[]="Http/1.1 200 OK\nContent-type: text/plain\n\n";
char headerError[]="Http/1.1 500 Internal Server Error\nContent-type: text/plain\n\n";

int process_request(char*,int);
void do_404(int);
void cannot_do(int);
char detectRequestType(struct stat*);
int ends_in_cgi(char* );

int do_ls(char*,int);
int countEntries(DIR*);
int cmpStr(const void* , const void* );		//For sorting files
int do_cat(char*,int);
int do_exec(char*,char*,int);
void closeSocket(int);

int longListDirContents(char*,FILE*);
char* calculatePermissions(struct stat*);
char* userName(struct stat*);
char* groupName(struct stat*);
char* getMTime();

int masterSock=-1;
int main()
{
	signal(SIGINT,closeSocket);
	//STEP-I **** Create a socket for the client
	masterSock=socket(AF_INET,SOCK_STREAM,0);
	if(masterSock==-1)
	{
		perror("Mastersocket not created");
		exit(1);
	}
	
	//STEP-II *** Populate Socket's DS for local IP and Port then bind
	struct sockaddr_in dest_addr;
	dest_addr.sin_family = AF_INET;			 //host byte order
	dest_addr.sin_port = htons(MY_PORT);		 //host to NW byte order
	inet_aton(MY_IP, &dest_addr.sin_addr); 	 //For converting string address into in_addr_t
	memset(&(dest_addr.sin_zero), '\0', sizeof(dest_addr.sin_zero)); //fill up the padding
	
	if(bind(masterSock, (struct sockaddr*)& dest_addr, sizeof(dest_addr))==-1)
	{
		perror("Unable to bind masterSocket");
		exit(1);
	}
	
	//STEP-III ***Create a connection queue and wait for clients
	
	if(listen(masterSock,BACK_LOG)==-1)
	{
		perror("Unable to listen()");
		exit(1);
	}
	
	//STEP-IV **** Accept client's HTML Request
	int client_sockfd;
	struct sockaddr_in client_addr;
	int client_len = sizeof(client_addr);
	char* msg = "\033[1m\033[35mServer waiting for a client request...\033[0m\033[39m\n";
	char* line="==========================================================================";
	while(1)
	{ //main accept() loop
		fprintf(stdout,"%s", msg);
		client_sockfd = accept(masterSock,(struct sockaddr*)&client_addr, &(client_len));
		fprintf(stdout,"%s\nClient connection established at socket on file descriptor %d\n", line, client_sockfd);
		//STEP-V ***** Process Request of client
		
		char rq[1024],request[200];
		memset(rq,'\0',1024);

		read(client_sockfd,rq,1023);

		//This method is taking too much time so I am reading directly in one call
		// int n,nread, nremaining;
		// for(nread=0, n=0; nread < 1024; nread += n)
		// {
		// 	nremaining = LINELEN - nread;
		// 	n = read(client_sockfd, &rq[nread], nremaining);
		// 	if(n==0) 
		// 		break;
		// 	if(n==-1)
		// 	{
		// 		perror("read failed");
		// 		exit(1);	//child will exit
		// 	}
		// }
		
		fprintf(stdout,"\n\n%s\n\n",rq);
		
		//for processing request
		sscanf(rq,"%[^\n]s",request);	//seperate 1st line of request
		int rv=process_request(request,client_sockfd);
		
		if(rv==-1)
		{
			fprintf(stderr,"\nClient request is not processed due to some error and response is sent to client accordingly.\n");
		}
		else
		{
			fprintf(stdout,"\nClient request processed successfully.\n");
		}	

		// if(close(client_sockfd)==-1)
		// {
		// 	perror("Unable to close client_sockfd");
		// 	exit(0);
		// }

		//No need to close client_sockfd because its FilePointer is already 
		//closed and if we try to close it here then this call will fail
		fprintf(stdout,"\nConnection with client %s:%d is finished.\n%s\n\n",inet_ntoa(client_addr.sin_addr),client_addr.sin_port,line);
		
	}//end of while
	
	//this will never execute
	close(masterSock);

	return 0;
}

int process_request(char* rq,int fd)
{
	char cmd[11], filestr[101], version[11];
	memset(cmd,'\0',11);
	memset(filestr,'\0',101);
	memset(version,'\0',11);
	
	/*
		REQUEST FORMAT:
		--------------------------------------------------
		=> 1st word should be 'GET'
		=> 2nd word is file_name preceded with / (/ = pwd)
			-> if it is regular then cat it
			-> if it is directory then list its contents
			-> if it is executable then exec it
		=> 3rd word should be HTTP Version string (Http/1.0)
		
		--------------------------------------------------
		Example of firefox Http request:
		GET /file HTTP/1.1
		Host: 192.168.85.129:49555
		User-Agent: Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:53.0) Gecko/20100101 Firefox/53.0
		Accept: text/html,application/xhtml+xml,application/xml;q=0.9,*//*;q=0.8
		Accept-Language: en-US,en;q=0.5
		Accept-Encoding: gzip, deflate
		Connection: keep-alive
		Upgrade-Insecure-Requests: 1
		--------------------------------------------------
		I will parse only 1st line
		--------------------------------------------------
	*/
	char ch;		//to read /
	
	sscanf(rq, "%10s %100s %10s", cmd, filestr,version);
	ch=filestr[0];
	char* file=NULL;
	int len=0;
	if(strlen(filestr)>1)
	{
		file=filestr+1;		//as filestr[0]='/'
		len=1;
	}
	fprintf(stdout,"file=\t%s",file);

	if(strcmp(cmd, "GET") != 0)	//GET not in request String
	{
		cannot_do(fd);
		return -1;
	}	
	else if(len!=0 && access(file,F_OK)==-1)		//File not found
	{
		do_404(fd);
		return -1;
	}
	else
	{
		if(len==0)		//for listing PWD
		{
			if(do_ls(file, fd)==-1)		//file=NULL is handled in do_ls()
			{
				FILE* fp=fdopen(fd,"w");
				fprintf(stderr,"There was an error while reading directory contents.\n");
				fprintf(fp,"%sThere was an error while reading directory contents.\n",headerError);
				fclose(fp);
			}
			return 0;
		}
		struct stat info={};
		if(stat(file, &info)==-1)
		{
			perror("error in stat");
			return -1;
		}
		char ch=detectRequestType(&info);
		if (ch=='d')
		{
			if(do_ls(file, fd)==-1)
			{
				FILE* fp=fdopen(fd,"w");
				fprintf(stderr,"There was an error while reading directory contents.\n");
				fprintf(fp,"%sThere was an error while reading directory contents.\n",headerError);
				fclose(fp);
			}
		}
		else if (ends_in_cgi(file))
		{
			char name[100]="rq-";
			strcat(name,file);
			if(do_exec(file,name, fd)==-1)
			{
				FILE* fp=fdopen(fd,"w");
				fprintf(stderr,"Unable to execute file (\"%s\") due to some error.\n",file);
				fprintf(fp,"%sUnable to execute file (\"%s\") due to some error.\n",file,headerError);
				fclose(fp);
			}
		}
		else if (ch=='-')
		{
			if(do_cat(file, fd)==-1)
			{
				FILE* fp=fdopen(fd,"w");
				fprintf(stderr,"There was an error in opening or reading file (\"%s\").\n",file);
				fprintf(fp,"%sThere was an error in opening or reading file (\"%s\").\n",file,headerError);
				fclose(fp);
			}
		}
	}
	return 0;
}

void do_404(int fd)
{
	FILE *fp = fdopen(fd, "w");
	fprintf(fp, "Http/1.1 404 Not Found\n");
	fprintf(fp, "Content-type: text/plain\n\n");
	fprintf(fp, "Requested item not found\n");
	fclose(fp);
}

void cannot_do(int fd)
{
	FILE *fp = fdopen(fd, "w");
	fprintf(fp, "Http/1.1 400 Bad Request\n");
	fprintf(fp, "Content-type: text/plain\n\n");
	fprintf(fp, "Http request format is invalid\n");
	fclose(fp);
}

char detectRequestType(struct stat* info)
{
	int type=info->st_mode & 0170000;

	//For fileType
	if (type == 0040000)
		return 'd';
	else if (type == 0100000)
		return '-';
	
	return 0;
}

int ends_in_cgi(char* file)
{
	if(strstr(file,".cgi\0")!=NULL)
		return 1;
	return 0;
}

int is_html(char* file)
{
	if(strstr(file,".html\0")!=NULL)
		return 1;
	return 0;
}

int do_ls(char* file, int fd)
{
	int rv=0;
	write(fd,headerText,strlen(headerText));
	FILE *fp = fdopen(fd, "w");
	if(file==NULL)
		rv=longListDirContents(".",fp);
	else
		rv=longListDirContents(file,fp);
	fclose(fp);

	// using ls command
	// int cpid=fork();
	// if(cpid==-1)
	// {
	// 	perror("fork failed");
	// 	return -1;
	// }
	// else if(cpid==0)
	// {	//Child
	// 	write(fd,headerText,strlen(headerText));
	// 	dup2(fd,1);
	// 	dup2(fd,2);
		
	// 	if(file==NULL)
	// 		execlp("ls","myls","-l",NULL);
	// 	else
	// 		execlp("ls","myls","-l",file,NULL);
	// 	perror("directory listing failed");
	// 	exit(1);
	// }	
	// else
	// {
	// 	int status;
	// 	wait(&status);
	// }
	
	return rv;
}

int do_cat(char* file, int fd)
{
	FILE* fp=fopen(file,"r");
	if(fp==NULL)
	{
		fprintf(fp,"%sUnable to open file(\"%s\") for reading.\n",file,headerError);
		fprintf(stderr,"Unable to open file(\"%s\") for reading",file);
		fclose(fp);
		perror("");
		return -1;
	}
	if(is_html(file)==1)
		send(fd,headerHtml,strlen(headerHtml),0);
	else
		send(fd,headerText,strlen(headerText),0);
	
	char buff[LINELEN];
	memset(buff,'\0',LINELEN);

	while(fgets(buff, LINELEN, fp))
	{
		send(fd,buff,strlen(buff),0);
	}

	fclose(fp);
	return 0;
}

int do_exec(char* file,char* name, int fd)
{
	int cpid=fork();
	int status;
	switch(cpid)
	{
		case -1:
			perror("Fork failed");
			return -1;
		case 0:
			//we have to redirect process_IO to client_sockfd(fd)
			write(fd,headerText,strlen(headerText));
			dup2(fd,1);
			dup2(fd,0);
			dup2(fd,2);
			execl(file,name,NULL);		//name of child(2nd parameter) will also be argList[0]
			perror("execvp failed");
			exit(1);
		default:
			if(waitpid(cpid,&status,0)==-1)
				perror("waitpid failed");

			return status;
	}
	return -1;		//this will be executed only in case of child
}

//SIGINT Handler
void closeSocket(int sig)
{
	fprintf(stdout,"\n\033[1m\033[31mClosing master-socket...\033[0m\033[39m\n");
	if(close(masterSock)==-1)
		perror("Unable to close master-socket");
	exit(0);
}


//For long-listing directories
int cmpStr(const void* s1, const void* s2)
{
	return strcasecmp(*((const char**)s1),*((const char**)s2));	
}

int longListDirContents(char* f,FILE* fp)
{
	DIR* dp;
	errno=0;
	dp=opendir(f);
	if(dp==NULL && errno!=0){	//error
		fprintf(fp,"\nError opening file %s",f);
		perror("");			
		return -1;
	}

	struct dirent * entry;
	errno=0;
	
	int fileCount=countEntries(dp);
	int i=0;
	char** files=(char**)malloc(sizeof(char*)*fileCount);
	while((entry = readdir(dp))!=NULL)
	{
		if(entry->d_name[0]=='.')			//don't display hidden file
		{
			errno=0;
			continue;
		}
		files[i++]=entry->d_name;
		errno=0;
	}
	if (entry == NULL && errno != 0) 	//Error
	{	
		perror("Error reading directory");
		return -1;
	} 
	//File completed
	if(fileCount>0){
		
		if(fileCount>1){
			qsort(files,fileCount,sizeof(char*),cmpStr);
		}
		
		//Without this strcat will change original argv[] in main and multifile won;t work
		char tF[strlen(f)+2];		
		strcpy(tF,f);

		char* path=strcat(tF,"/");
				
		for(int i=0;i<fileCount;i++)
		{
			struct stat info;
			
			char file[256];
			strcpy(file,path);			
			strcat(file,files[i]);

			int s=lstat(file, &info);
			if(s==0)
			{
				char* perm=calculatePermissions(&info);
				char* user=userName(&info);
				char* group=groupName(&info);				
				char* time=getMTime(info.st_mtime);
				fprintf(fp,"%8ld %s %3ld %-10s%-10s %10ld %s ",info.st_ino,perm,info.st_nlink,user,group,info.st_size,time);
				//for printing file name with proper color
				if(perm[0]=='-')
					fprintf(fp,"%s",files[i]);
				else if(perm[0]=='d')
					fprintf(fp,"%s",files[i]);
				else if(perm[0]=='c' || perm[0]=='b')
					fprintf(fp,"%s",files[i]);
				else if(perm[0]=='p')
					fprintf(fp,"%s",files[i]);
				else if(perm[0]=='l')		// || info.st_nlink>0
				{
					fprintf(fp,"%s",files[i]);
					char* realFile=realpath(file,NULL);
					fprintf(fp," -> %s",realFile);
					free(realFile);
				}
				free(perm);
				free(time);
			}
			else if(s==-1){
				perror("Error getting permissions");
				return -1;
			}
			if(i!=fileCount)
				fprintf(fp,"\n");
		}
		free(files);
	}
	closedir(dp);
	return 0;
}

char* getMTime(long int time)
{
	char* t=ctime(&time);
	char* timeStr=(char*)malloc(sizeof(char)*12);
	char* tok=strtok(t," ");

	int i=1;
	char* timeTok;
	char* mtime=(char*)malloc(sizeof(char)*8);
	// strcat(mtime," ");
	// mtime[0]='\0';
	while( tok != NULL ) 
   {
   		// printf("\n\n%s",tok);
      	if(i==2)
		{
			strcpy(timeStr,"");
			strcat(timeStr,tok);
			
			timeStr[3]=' ';
			timeStr[4]='\0';
		}
		if(i==3)
		{
			int day=atoi(tok);
			
			if(day<=9)
				strcat(timeStr," ");
			
			strcat(timeStr,tok);			
		}
		if(i==4)
		{
			strcpy(mtime," ");
			int j=1;

			timeTok=strtok(tok,":");
			while( timeTok != NULL ) 
			{
				
				if(j==1 || j==2)
				{
					strcat(mtime,timeTok);
					if(j==1)
						strcat(mtime,":");					
				}
				timeTok=strtok(NULL,":");
				j++;
			}
			strcat(timeStr,mtime);
			//printf("\n\n%s",timeStr);			
		}
    
		tok = strtok(NULL, " ");
		i++;
   }
   
   return timeStr;
}

char* calculatePermissions(struct stat* info)
{
	int type=info->st_mode & 0170000;
	char* perm=(char*)malloc(sizeof(char)*11);
	strcpy(perm,"----------");
	//For fileType
	if (type == 0010000)
		perm[0]='p';
	else if (type == 0020000)
		perm[0]='c';
	else if (type == 0040000)
		perm[0]='d';
	else if (type == 0060000)
		perm[0]='b';
	else if (type == 0100000)
		perm[0]='-';
	else if (type == 0120000)
		perm[0]='l';
	else if (type == 0140000)
		perm[0]='s';
	//For user permissions
	if((info->st_mode & 0000400) == 0000400)
		perm[1]='r';
	if((info->st_mode & 0000200) == 0000200)
		perm[2]='w';
	if((info->st_mode & 0000100) == 0000100)
		perm[3]='x';
	//For group permissions
	if((info->st_mode & 0000040) == 0000040)
		perm[4]='r';
	if((info->st_mode & 0000020) == 0000020)
		perm[5]='w';
	if((info->st_mode & 0000010) == 0000010)
		perm[6]='x';
	//For others permissions
	if((info->st_mode & 0000004) == 0000004)
		perm[7]='r';
	if((info->st_mode & 0000002) == 0000002)
		perm[8]='w';
	if((info->st_mode & 0000001) == 0000001)
		perm[9]='x';
	
	//For Special permisiions
	if((info->st_mode & 0004000) == 0004000)
	{
		if(perm[3]=='x')
			perm[3]='S';
		else
			perm[3]='s';
	}
	if((info->st_mode & 0002000) == 0002000)
	{
		if(perm[6]=='x')
			perm[6]='S';
		else
			perm[6]='s';
	}
	if((info->st_mode & 0001000) == 0001000)
	{
		if(perm[9]=='x')
			perm[9]='T';
		else
			perm[9]='t';
	}
	return perm;
}

int countEntries(DIR* dp)
{
	int count=0;
	struct dirent * entry;
	while((entry = readdir(dp))!=NULL)
	{
		if(entry->d_name[0]=='.')			//don't count hidden files
		{
			errno=0;
			continue;
		}
		count++;
		errno=0;
	}
	
	if (entry == NULL && errno != 0) 	//Error
	{	
		perror("Error reading directory");
		exit(0);
	}
	else
	{
		rewinddir(dp);			//rewind directory so that all these files can be displayed
	}

	return count;
}

char* userName(struct stat* info)
{
	errno = 0;
	struct passwd * pwd = getpwuid(info->st_uid);
	if (pwd == NULL){
		if (errno == 0)
			fprintf(stderr,"Record not found in passwd file.\n");
		else
			perror("getpwuid failed");
	}
	
	char* name=pwd->pw_name;
	return name;
}

char* groupName(struct stat* info)
{
	errno = 0;
	struct group * grp = getgrgid(info->st_gid);
	if (grp == NULL){
		if (errno == 0)
			fprintf(stderr,"Record not found in passwd file.\n");
		else
			perror("getgrgid failed");
	}
	char* name=grp->gr_name;
	return name;
}