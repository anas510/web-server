All versions handle Http GET request from client browser.
They can do following 4 tasks:

=>	List directory contents

=>	Read Regular files

=>	Send HTML Response (for html files)

=>	Execute *.cgi files and send output in response


Version-1:
This is a connection-oriented Iterative web server

Version-2:
This is a connection-oriented Concurrent web server (using FORK)

Version-3:
This is a connection-oriented Concurrent web server (using pthread)

Version-4:
This is a concurrent version using select() call.